#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <openssl/rand.h>
#include <openssl/bio.h>
#include <openssl/evp.h>
#include <openssl/sha.h>
#include "pass_store.h"

#define SALT_LEN 8

// Every 3 bytes of salt encoded as 4 characters + possible padding
#define SALT_LEN_BASE64 (SALT_LEN/3 + 1) * 4
#define SHA512_DIGEST_LENGTH_BASE64 (SHA512_DIGEST_LENGTH/3 + 1) * 4

#define MAX_USER_LEN 32
#define PASS_FILE_PATH "passwords"

typedef struct user_pass_s {
	// NULL-terminated username string
	// if username is empty, consider the entry removed
	char username[MAX_USER_LEN];
	// binary password hash (no encoding)
	uint8_t pass_hash[SHA512_DIGEST_LENGTH];
	// NULL-terminated Base64 encoded salt string
	char salt[SALT_LEN_BASE64 + 1];
} user_pass_t;

static int __pass_store_load(user_pass_t **passwords_out, size_t *num_pass_out)
{
	FILE *file_ptr = NULL;
	file_ptr = fopen(PASS_FILE_PATH, "r");
	if (file_ptr == NULL) {
		fputs("Password file read error\n", stderr);
    return -1;
	}

  // Get number of users in file:
  int num_users = 0;
  while (!feof(file_ptr)) {
    char ch = fgetc(file_ptr);
    if (ch == '\n') {
      num_users++;
    }
  }

  fseek (file_ptr, 0, SEEK_SET);
  user_pass_t * users = malloc(sizeof(user_pass_t) * num_users);
	char * password_buf = malloc(SHA512_DIGEST_LENGTH_BASE64);
	
  num_users = 0;

	char * token = NULL;
	int field = 0;
  char line [1024];

	while(fscanf(file_ptr, "%s\n", line) != EOF){ //%s:$6$%s$%
		token = strtok(line, ":$");
		while(token){
      if (field == 0) {
					memcpy(users[num_users].username, token, MAX_USER_LEN);
          users[num_users].username[MAX_USER_LEN - 1] = '\0';
      } else if (field == 2) {
					memcpy(users[num_users].salt, token, SALT_LEN_BASE64 + 1);
          users[num_users].salt[SALT_LEN_BASE64] = '\0';
      } else if (field == 3) {
					BIO *enc_bio = BIO_new_mem_buf(token, SHA512_DIGEST_LENGTH_BASE64);
					BIO *b64_bio = BIO_new(BIO_f_base64());
					BIO_push(b64_bio, enc_bio);
					BIO_set_flags(b64_bio, BIO_FLAGS_BASE64_NO_NL);
					BIO_read(b64_bio, users[num_users].pass_hash, SHA512_DIGEST_LENGTH);
					BIO_free_all(b64_bio);
      }
			token = strtok(NULL, ":$");
			field++;
    }
    field = 0;
    num_users ++;
	}

  *passwords_out = users;
	*num_pass_out = num_users;
	free(password_buf);
	fclose(file_ptr);
	return 0;
}


static int __pass_store_save(user_pass_t *passwords, size_t num_pass, int append)
{
  // Convert password to base 64:
  BIO *digest_buff = BIO_new(BIO_f_base64());
  BIO *mem_sink = BIO_new(BIO_s_mem());
  BIO_push(digest_buff, mem_sink);
  BIO_set_flags(digest_buff, BIO_FLAGS_BASE64_NO_NL);
  BIO_write(digest_buff, passwords -> pass_hash, SHA512_DIGEST_LENGTH);
  BIO_flush(digest_buff);
  char * pass_str = NULL;
  BIO_get_mem_data(mem_sink, &pass_str);

  FILE * pass_file = NULL;
  if (append == 1) {
    pass_file = fopen(PASS_FILE_PATH, "a");
  } else {
    pass_file = fopen(PASS_FILE_PATH, "w");
  }
  if (pass_file == NULL) {
    printf("Could not write to password file.");
    BIO_free_all(digest_buff);
    return -1;
  }

  fprintf(pass_file, "%s:$6$%s$%s\n", passwords -> username, passwords -> salt, pass_str);
  fclose(pass_file);
  BIO_free_all(digest_buff);
  return 0;
}

/*
* pass_store_add_user - adds a new user to the password store
*
* @username: NULL-delimited username string
* @password: NULL-delimited password string
*
* Returns 0 on success, -1 on failure
*/
int pass_store_add_user(const char *username, const char *password)
{
  // Check to see if user exists first:
  FILE *file_ptr = NULL;
	file_ptr = fopen(PASS_FILE_PATH, "r");
  // First see if file exists:
  if (file_ptr != NULL) {
    size_t num_users = 0;
    user_pass_t * users;
    // Error if file exists but couldn't load:
    if (__pass_store_load(&users, &num_users) != 0) {
      printf("Could not load users.\n");
      free(users);
      return -1;
    }
    // Error if username already exists:
    for(int i = 0; i < num_users; i++) {
      if (strncmp(users[i].username, username, MAX_USER_LEN) == 0) {
        printf("That username already exists. Please choose a different one.\n");
        free(users);
        return -1;
      }
    }
  }

  // Initialize user and set the username:
  user_pass_t new_user;
  memcpy(new_user.username, username, MAX_USER_LEN);
  new_user.username[MAX_USER_LEN - 1] = '\0';

  // Generate salt:
  unsigned char * saltBuffer = malloc(SALT_LEN);
  if (RAND_bytes(saltBuffer, SALT_LEN) == 0) {
    free(saltBuffer);
    printf("Could not generate random data.\n");
    return -1;
  }

  // Base-64 encode salt:
  BIO *saltBio = BIO_new(BIO_f_base64());
  BIO *mem_sink = BIO_new(BIO_s_mem());
  BIO_push(saltBio, mem_sink);
  BIO_set_flags(saltBio, BIO_FLAGS_BASE64_NO_NL);
  BIO_write(saltBio, saltBuffer, SALT_LEN);
  BIO_flush(saltBio);
  char *saltPtr = NULL;
  BIO_get_mem_data(mem_sink, &saltPtr);
  memcpy(new_user.salt, saltPtr, SALT_LEN_BASE64);
  new_user.salt[SALT_LEN_BASE64] = '\0';
  free(saltBuffer);

  // Concat b-64 salt to end of password: (-1 to get rid of \n)
  size_t pass_len = strlen(password) - 1 + SALT_LEN_BASE64;
  unsigned char * salted_pass = malloc(pass_len);
  memcpy(salted_pass, password, strlen(password) - 1);
  memcpy(salted_pass + strlen(password) - 1, new_user.salt, SALT_LEN_BASE64);

  // SHA-512 hash of the concated string:
  SHA512(salted_pass, pass_len, new_user.pass_hash);
  free(salted_pass);
  BIO_free_all(saltBio);

  // Add to passwords file:
  if (__pass_store_save(&new_user, 1, 1) != 0) {
    printf("Could not save to file.\n");
    return -1;
  }

  return 0;
}


/*
* pass_store_remove_user - removes a user from the password store
*
* @username: NULL-delimited username string
*
* Returns 0 on success, -1 on failure
*/
int pass_store_remove_user(const char *username)
{
  size_t num_users = 0;
  user_pass_t * users;
  if (__pass_store_load(&users, &num_users) != 0) {
    printf("Could not load users.\n");
    free(users);
    return -1;
  }

  int append = 0;
  for (int i = 0; i < num_users; i++) {
    if (strncmp(users[i].username, username, MAX_USER_LEN) != 0) {
      // Add to passwords file:
      if (__pass_store_save(&users[i], 1, append) != 0) {
        printf("Could not save to file.\n");
        free(users);
        return -1;
      }
      if (append == 0) {
        append = 1; // After the first time, it will append
      }
    }
  }
  free(users);
  return 0;
}


/* pass_store_check_password - check the password of a user
*
* @username: NULL-delimited username string
* @passwrod: NULL-delimited password string
*
* Returns 0 on success, -1 on failure
*/
int pass_store_check_password(const char *username, const char *password)
{
  size_t num_users = 10;
  user_pass_t * users;
  if (__pass_store_load(&users, &num_users) != 0) {
    printf("Could not load users.\n");
    free(users);
    return -1;
  }

  for (int i = 0; i < num_users; i++) {
    if (strncmp(users[i].username, username, MAX_USER_LEN) == 0) {
      // Concat salt to end of password:
      size_t pass_len = strlen(password) - 1 + SALT_LEN_BASE64;
      unsigned char * salted_pass = malloc(pass_len);
      memcpy(salted_pass, password, strlen(password) - 1);
      memcpy(salted_pass + strlen(password) - 1, users[i].salt, SALT_LEN_BASE64);

      // SHA-512 hash of the concated string:
      uint8_t hash [SHA512_DIGEST_LENGTH];
      SHA512(salted_pass, pass_len, hash);
      free(salted_pass);
      if (memcmp(users[i].pass_hash, hash, SHA512_DIGEST_LENGTH) == 0) {
        free(users);
        printf("Password match!\n");
        return 0;
      } else {
        free(users);
        printf("Password did not match.\n");
        return -1;
      }
    }
  }
  free(users);
  printf("No user found.\n");
  return -1;
}
